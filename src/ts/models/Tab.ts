import { Component } from "../core/component";

export class Tab {
    constructor(
        public name: string,
        public component: Component
    ) {}
}