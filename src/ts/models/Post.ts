export class Post {
    constructor(
        public title: string = null,
        public fulltext: string = null,
        public type: 'news' | 'note' = null,
        public date: string = new Date().toLocaleDateString(),
        public id?: string
    ){}
}