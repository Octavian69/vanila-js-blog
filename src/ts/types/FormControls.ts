import { Validator } from "./Validator";

export type FormControls = {
    [key: string]: {
        value: any,
        validators: Array<Validator>
    }
}
