export type ControlValidate = {
    message: string;
    valid: boolean;
}