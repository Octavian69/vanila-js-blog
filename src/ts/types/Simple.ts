export type Simple<T> = {
    [key: string]: T
}