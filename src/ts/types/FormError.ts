import { ControlValidate } from "./ControlValidate";

export class FormError {
    
    message: string;
    valid: boolean;

    constructor(
        public controlName: string,
        public controlTitle: string,
        validate: ControlValidate
    ) {
        const { message, valid } = validate;

        this.message = message.replace('{{replace}}', this.controlTitle);
        this.valid = valid;
    }
}