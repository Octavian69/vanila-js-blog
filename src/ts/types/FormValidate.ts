import { FormError } from "./FormError";

export class FormValidate {
    constructor(
        public valid: boolean,
        public errors: FormError[]
    ) {}
}