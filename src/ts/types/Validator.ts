import { ControlValidate } from "./ControlValidate";


export type Validator = (v: any) => ControlValidate;