import { Post } from "../models/Post";
import { Simple } from "../types/Simple";
import { StorageService } from "../services/storage.service";

const tags: Simple<string> = {
    note: 'Заметка',
    news: 'Новость'
}

export function isFavoritePost(id: string): boolean {
    const favorites: string[] = StorageService.getItem('favorites') || [];

    return favorites.includes(id);
}



export function renderPost(post: Post, options: Simple<any> = {}): string {
    const tag: string = tags[post.type];
    const tagClass: string = Object.is(post.type, 'news') ? 'tag-blue' : '';
    const isFavorite: boolean = isFavoritePost(post.id)
    const btnClass: string =  isFavorite ? 'button-danger' : 'button-primary';
    const btnText: string = isFavorite ? 'Удалить' : 'Сохранить';

    const addButton: string = `
        <button  
            class = 'button-small button-round ${btnClass}'
            data-post-id = "${post.id}"
        >${btnText}</button>
    `;

    return `
        <div class="panel">
        <div class="panel-head">
          <p class="panel-title">${post.title}</p>
          <ul class="tags">
            <li class="tag ${tagClass} tag-rounded">${tag}</li>
          </ul>
        </div>
        <div class="panel-body">
          <p class="multi-line">${post.fulltext}</p>
        </div>
        <div class="panel-footer w-panel-footer">
          <small>${post.date}</small>
          ${options.addButton ? addButton : ''}
        </div>
      </div>
    `
}