import { Component } from "../core/component";
import { StorageService } from "../services/storage.service";

export class HeaderComponent extends Component {

    static instance: HeaderComponent;

    viewState: 'hide' | 'show';
    $startBtn: HTMLElement;

    constructor(id: 'header') {
        super(id);

        if(!HeaderComponent.instance) {
            HeaderComponent.instance = this;
        }

        return HeaderComponent.instance;
    }

    init(): void {
        this.initStartButton();
    }

    private initStartButton(): void {
        this.$startBtn = this.getChild('.js-header-start');

        const visited: boolean = StorageService.getItem('visited');

        if(visited) this.setClass('hide', 'add');
        else this.$startBtn.addEventListener('click', this.buttonTrigger.bind(this));
       
    }

    private buttonTrigger() {
        StorageService.setItem('visited', true);
        this.hide();
    }
}