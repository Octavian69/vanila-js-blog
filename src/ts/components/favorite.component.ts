import { Component } from "../core/component";
import { StorageService } from "../services/storage.service";
import { LoaderComponent } from "./loader.component";
import { ApiService } from "../services/api.service";
import { Post } from "../models/Post";
import { renderPost } from "../handlers/posts.handlers";

export class FavoriteComponent extends Component {

    backSub$: () => void;

    static instance: FavoriteComponent;

    constructor(id: 'favorite', public state: { loader: LoaderComponent }) {
        super(id);

        if(!FavoriteComponent.instance) {
            FavoriteComponent.instance = this;
        };

        return FavoriteComponent.instance;
    }

    init(): void {

    }

    onInit(): void {
        const favorites: string[] = StorageService.getItem('favorites') || [];

        this.renderPostIds(favorites);
        this.initSubs();
    }

    onDestroy(): void {
        this.$elem.innerHTML = '';
    }

    initSubs(): void {
        const elems$: NodeList = this.$elem.querySelectorAll('[data-favorite-id]');

        if(elems$.length) {
            elems$.forEach((elem$: HTMLElement) => {
                elem$.addEventListener('click', this.getPost.bind(this))
            })
        }
    }

    initBackBtnSub(): void {
        const btn = this.getChild('.js-favorites-back') as HTMLButtonElement;
        const backSub$ = this.back.bind(this);

        btn.addEventListener('click', backSub$);
    }

    back(): void {
        const btn = this.getChild('.js-favorites-back') as HTMLButtonElement;

        btn.removeEventListener('click', this.backSub$);
        this.$elem.innerHTML = '';
        this.onInit();
    }

    async getPost(e: Event) {
        e.preventDefault();

        const $elem = e.target as HTMLElement;
        const { favoriteId } = $elem.dataset;
        
        this.$elem.innerHTML = '';

        this.state.loader.setClass('hide', 'remove');
        
        const post: Post = await ApiService.fetchPostById(favoriteId);
        
        this.$elem.innerHTML = this.getBackButton() + renderPost(post);

        this.initBackBtnSub();

        this.state.loader.setClass('hide', 'add');
        
    }


    getBackButton(): string {
        return `
            <button 
                class="js-favorites-back button-small button-round button-back"
            >Назад</button>
        `;
    }


    renderPostIds(favorites: string[]): void {
        let html: string;

        if(favorites.length) {
            
            const list: string[] = favorites.map((id: string) => {
                return `
                <li>
                    <a class='js-link' data-favorite-id="${id}" href="#">${id}</a>
                </li>`;
            })
            
            html = `<ul>${list.join('')}</ul>`
        } else {
            html = '<p class = "center">Список пуст</p>';
        }
        
        this.$elem.innerHTML = html;
    }
}