import { Component } from "../core/component";

export class LoaderComponent extends Component {

    static instance: LoaderComponent;

    constructor(id: 'loader') {
        super(id);

        if(!LoaderComponent.instance) {
            LoaderComponent.instance = this;
        }

        return LoaderComponent.instance;
    }
}