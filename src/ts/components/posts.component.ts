import { Component } from "../core/component";
import { ApiService } from "../services/api.service";
import { TransformService } from "../services/transform.service";
import { Post } from "../models/Post";
import { Simple } from "../types/Simple";
import { LoaderComponent } from "./loader.component";
import { StorageService } from "../services/storage.service";
import { renderPost, isFavoritePost } from "../handlers/posts.handlers";



export class PostsComponent extends Component {
    
    static instance: PostsComponent;

    sub: any;
    loader: LoaderComponent

    constructor(id: 'posts', public state: {loader: LoaderComponent}) {
        super(id);

        if(!PostsComponent.instance) {
            PostsComponent.instance = this;
        }

        return PostsComponent.instance;
    }

    async onInit() {
        this.initFavoriteSub();

        const { loader } = this.state;
        loader.setClass('hide', 'remove');

        const response = await ApiService.fetchPosts();
        const posts: Post[] = TransformService.fbObjectToArray(response);

        this.render(posts);

        loader.setClass('hide', 'add');
    }

    onDestroy(): void {
        const { loader } = this.state;
        this.$elem.removeEventListener('click', this.sub);
        this.$elem.innerHTML = '';
        loader.setClass('hide', 'add');
    }

    private initFavoriteSub(): void {
        const sub = this.clickParent.bind(this);
        this.sub = sub;
        this.$elem.addEventListener('click', sub);
    }

    private clickParent(e: Event) {
        const el$ = e.target as HTMLElement;
        const id: string = el$.dataset.postId;

        if(id) {
            this.triggerFavorite(el$ as HTMLButtonElement, id);
        }
    }

    public triggerFavorite(el$: HTMLButtonElement, id: string) {
        const favorites: string[] = StorageService.getItem('favorites') || [];
        const isFavorite: boolean = isFavoritePost(id)

        const addClass: string = isFavorite ? 'button-primary' : 'button-danger';
        const removeClass: string = isFavorite ? 'button-danger' : 'button-primary';

        el$.classList.add(addClass);
        el$.classList.remove(removeClass);
        el$.textContent = !isFavorite ? 'Удалить' : 'Сохранить';
        
        if(isFavorite) {
            const idx: number = favorites.indexOf(id);
            favorites.splice(idx, 1);
        } else {
            favorites.push(id);
        }

        StorageService.setItem('favorites', favorites);
    }



    private renderPost(post: Post): string {
        return renderPost(post, { addButton: true });
    }

    private render(posts: Post[]): void {
        const htmlArray: string[] = posts.map((post: Post) => this.renderPost(post));

        this.$elem.innerHTML = htmlArray.join('');
    }
}