import { Component } from "../core/component";
import { Form } from "../core/form";
import { FormControls } from "../types/FormControls";
import { Validators } from "../core/validators";
import { FormValidate } from "../types/FormValidate";
import { Simple } from "../types/Simple";
import { Post } from "../models/Post";
import { ApiService } from "../services/api.service";

export class CreateComponent extends Component {

    static instance: CreateComponent;

    form: Form;

    constructor(id: 'create') {
        super(id);
   
        if(!CreateComponent.instance) {
            CreateComponent.instance = this;
        }

        return CreateComponent.instance;
    }

    init(): void {
        this.initForm();
        this.initSubmitSub();
    }

    initForm(): void {
        const controls: FormControls = {
            title: { value: '',   validators: [Validators.required, Validators.minLength(10)]},
            fulltext: { value: '',   validators: [Validators.required]},
            type: { value: 'note', validators: [] }
        };

        this.form = new Form(this.$elem as HTMLFormElement, controls);
    }

    initSubmitSub(): void {
        this.$elem.addEventListener('submit', this.submitHandler.bind(this))
    }

    async submitHandler(e: Event) {
        e.preventDefault();
    
        const validate: FormValidate = this.form.isValid();
    
        if(validate.valid) {
            const value: Simple<any> = this.form.value();
            const post: Post = Object.assign(new Post, value);
    
            const response = await ApiService.createPost(post);
    
            console.log(response);
    
            this.form.clear();
        }
    }
}