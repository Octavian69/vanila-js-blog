import { Component } from "../core/component";
import { StorageService } from "../services/storage.service";
import { Tab } from "../models/Tab";

export class NavigationComponent extends Component {

    tabs: Tab[];
    
    static instance: NavigationComponent;
    
    constructor(id: 'navigation') {
        super('navigation');
        
        
        if(!NavigationComponent.instance) {
            NavigationComponent.instance = this;
        }

        
        return NavigationComponent.instance;
    }

    init(): void {
        this.initPage();
    }

    private initPage(): void {
        this.initTabSubs();
    }

    public initTabs(tabs: Tab[]) {
        this.tabs = tabs;

        const currentPage = this.getCurrentPage();

        this.setPage(currentPage);
    }

    private initTabSubs(): void {
        const tabs: NodeListOf<Element> = this.$elem.querySelectorAll('.tab');

        Array.from(tabs).forEach((tab: HTMLElement) => {
            const page = tab.getAttribute('data-name') as 'posts' | 'create' | 'favorite';

            tab.addEventListener('click', (e: Event) => {
                e.preventDefault();
                    
                const currentPage = this.getCurrentPage();
                const isCurrent: boolean = Object.is(currentPage, page);

                if(!isCurrent) this.setPage(page)
            } );
        })
    }

    private getCurrentPage(): 'posts' | 'create' | 'favorite' {
        return StorageService.getItem('currentPage') || 'create';
    }

    private setPage(page: 'posts' | 'create' | 'favorite'): void {
        
        const currentPage = this.getCurrentPage();

        const navTab: HTMLElement = this.getNavTab(currentPage);
        const tab: Tab = this.getComponent(currentPage);
        
        navTab.classList.remove('active');
        tab.component.hide();
        
        this.setTab(page);
        this.setNavTab(page);
        StorageService.setItem('currentPage', page);
        
    }

    private setTab(page: 'posts' | 'create' | 'favorite') {
        const tab: Tab = this.tabs.find((tab: Tab) => tab.name === page);

        tab.component.show();
    }

    private setNavTab(page: 'posts' | 'create' | 'favorite'): void {
        const navTab: HTMLElement = this.getNavTab(page);

        navTab.classList.add('active');
    }

    private getComponent(page: 'posts' | 'create' | 'favorite'): Tab {
        return this.tabs.find((tab: Tab) => tab.name === page);
    }

    private getNavTab(page: 'posts' | 'create' | 'favorite'): HTMLElement {
        return this.getChild(`[data-name=${page}]`);
    }
}