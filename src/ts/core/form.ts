import { FormControls } from "../types/FormControls";
import { Simple } from "../types/Simple";
import { FormError } from "../types/FormError";
import { ControlValidate } from "../types/ControlValidate";
import { Validator } from "../types/Validator";
import { FormValidate } from "../types/FormValidate";

const Titles: Simple<string> = {
    'title': 'Название',
    'fulltext': 'Текст',
    'note': 'Тип'
}

export class Form {

    constructor(
        public form: HTMLFormElement,
        public controls: FormControls
    ){}

    value(): Simple<any> {
        return Object.keys(this.controls).reduce((accum: Simple<any>, control: string) => {
            accum[control] = this.form[control].value;

            return accum;
        }, {});
    }

    isValid(): FormValidate {
        
        const value = this.value();

        this.clearErrors();

        const errors: FormError[] = Object.keys(this.controls).reduce((accum: FormError[], control: string) => {
            
            const validators: Validator[] = this.controls[control].validators;
            let isValid: boolean = true;

            validators.forEach((validator: Validator) => {
                const controlValue = value[control];
                const validate: ControlValidate = validator(controlValue);

                if(!validate.valid && isValid) {
                    const controlTitle: string = Titles[control]
                    const error: FormError = new FormError(control, controlTitle, validate);

                    accum.push(error);

                    isValid = false;
                }
            })
            
            return accum;
        }, []);

        const isValid: boolean = errors.length > 0 ? false : true;

        const validate: FormValidate = new FormValidate(isValid, errors);

        isValid ? this.clearErrors() : this.setErrors(errors)

        return validate;
    }

    setErrors(errors: FormError[]): void {
        errors.forEach((error: FormError) => {
            const { controlName, message } = error;
            const parent$: HTMLElement = this.form[controlName].parentNode;
            const nodeError: string = this.createNodeError(message);
            
            this.form[controlName].classList.add('invalid');
            parent$.insertAdjacentHTML('beforeend', nodeError);
            
        })
    }

    createNodeError(message: string): string {
        return `<p class="validation-error">${message}</p>`;
    }

    clearErrors(): void {
        Object.keys(this.controls).forEach((control: string) => {
            const parent$: HTMLElement = this.form[control].parentNode;
            const errorElem$: HTMLElement = parent$.querySelector('.validation-error');

            this.form[control].classList.remove('invalid');

            if(errorElem$) errorElem$.remove();
        })
    }

    clear(): void {
        Object.keys(this.controls).forEach((control: string) => {
            this.form[control].value = this.controls[control].value;
        })
    }
}

'invalid'
'validation-error'