
import { HeaderComponent } from "../components/header.component";
import { NavigationComponent } from "../components/navigation.component";
import { CreateComponent } from "../components/create.component";
import { PostsComponent } from "../components/posts.component";
import { FavoriteComponent } from "../components/favorite.component";
import { Tab } from "../models/Tab";
import { LoaderComponent } from "../components/loader.component";


//init

new HeaderComponent('header');

const navigation = new NavigationComponent('navigation');
const loader = new LoaderComponent('loader');

const create = new CreateComponent('create');
const posts = new PostsComponent('posts', { loader });
const favorite = new FavoriteComponent('favorite', { loader });

navigation.initTabs([
    new Tab('create', create),
    new Tab('posts', posts),
    new Tab('favorite', favorite)
])
