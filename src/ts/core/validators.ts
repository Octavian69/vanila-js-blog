import { Validator } from "../types/Validator";
import { ControlValidate } from "../types/ControlValidate";

export class Validators {

    static required(value: string): ControlValidate {
        const valid: boolean = !!(value && value.trim());
        const message: string = 'Поле "{{replace}}" обязательно для заполнения';

        return { valid, message };
    }

    static minLength(requiredLength: number): Validator {
        return (value: string): ControlValidate => {
            const valid: boolean = value.length > requiredLength;
            const message: string = `Поле "{{replace}}" не может быть меньше ${requiredLength} символов.`;

            return { valid, message };
        } 
    }
}