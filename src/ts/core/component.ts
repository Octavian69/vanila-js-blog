export class Component {
    $elem: HTMLElement;

    constructor(id: string) {
        this.$elem = document.getElementById(id);
        this.initFadeStyles();
        this.init();
    }

    init() {}

    initFadeStyles() : void {
        this.$elem.style.transition = 'opacity .3s linear'
    }

    onInit() {}

    onDestroy() {}

    show(): void {
        setTimeout(_ => {
            this.$elem.classList.remove('hide');
            this.setStyle('opacity', '1');
            this.onInit();
        } , 300);
    }

    hide(): void {
        this.setStyle('opacity', '0');

        setTimeout(_ => {
            this.$elem.classList.add('hide');
            this.onDestroy();
        }, 300);
    }

    getChild(selector: string): HTMLElement {
        return this.$elem.querySelector(selector);
    }

    setStyle(key: any, value: string): void {
        this.$elem.style[key] = value;
    }

    setClass(className: string, action: 'add' | 'remove'): void {
        this.$elem.classList[action](className);
    }
}