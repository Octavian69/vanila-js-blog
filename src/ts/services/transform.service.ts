import { Simple } from "../types/Simple";

export class TransformService {
    static fbObjectToArray(obj: Simple<any>): any[] {
        const array: any[] = Object.keys(obj).map((key: string) => {
            const transform = obj[key];

            transform.id = key;

            return transform;
        })

        return array;
    }
}