class Service {
    constructor() {}

    setItem<T>(key: string, value: T, storage: 'sessionStorage' | 'localStorage' = 'sessionStorage'): void {
        const payload: string = JSON.stringify(value)

        window[storage].setItem(key, payload);
    }

    getItem<T>(key: string, storage: 'sessionStorage' | 'localStorage' = 'sessionStorage'): T {
       const value: string = window[storage].getItem(key);

       return JSON.parse(value);
    }
}

export const StorageService = Object.freeze(new Service);