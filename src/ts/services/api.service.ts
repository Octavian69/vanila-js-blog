import { Post } from "../models/Post";

class Service {
    constructor(public url: string) {}
    
    completeUrl(path: string): string {
        return `${this.url}/${path}`;
    }

    async useRequest(request: Request) {
        const response = await fetch(request);
        const json = await response.json();

        return json;
    }

    async fetchPosts() {
        try {

            const url: string = this.completeUrl('posts.json');

            const request = new Request(url, { method: 'get' });
            const response = await this.useRequest(request);

            return response;

        } catch(e) {
            console.error(e);
        }
    }

    async createPost(post: Post) {
        try {
            const url: string = this.completeUrl('posts.json');
            const settings: RequestInit = {method: 'post', body: JSON.stringify(post)};

            const request: Request = new Request(url, settings);
            const response = await this.useRequest(request);

            return response;

        } catch(e) {
            console.error(e);
        }
    }

    async fetchPostById(id: string ) {
        const url: string = this.completeUrl(`posts/${id}.json`);

        const request: Request = new Request(url, { method: 'get' });
        const response = await this.useRequest(request);

        return response;
    }
}

export const ApiService = Object.freeze(new Service('https://vanila-js-blog.firebaseio.com'));